package com.jt;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.com.jt.jt.dao") //主要告诉mapper的包路径,会自动的完成包扫描
public class SpringbootDemo2MybatisApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootDemo2MybatisApplication.class, args);
	}

}
