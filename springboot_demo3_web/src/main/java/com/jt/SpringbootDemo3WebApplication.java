package com.jt;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.com.jt.jt.com.com.jt.jt.mapper")
public class SpringbootDemo3WebApplication {

	public static void main(String[] args) {

		SpringApplication.run(SpringbootDemo3WebApplication.class, args);
	}

}
