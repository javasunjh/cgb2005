package com.jt.controller;

import com.jt.pojo.User;
import com.jt.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

//@RestController   //json  字符串本身   不经过视图解析器
@Controller
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 需求：用户通过http://localhost:8090/findAll
     * 跳转页面路径:userList.jsp
     * 页面取值信息: el表达式:${userList} 从域中取值.
     *              在页面跳转之前应该将userList数据保存到域中 key就是userList.
     */
    @RequestMapping("/findAll")
    public String findAll(Model model){  //利用model对象将数据保存到request对象中.

        //1.查询数据库 获取list集合信息
        List<User> userList = userService.findAll();
        model.addAttribute("userList",userList);
        System.out.println(userList);
        return "userList";
    }

    /**
     * 跳转到ajax.jsp页面
     */
    @RequestMapping("/ajax")
    public String ajax(){

        return "ajax";
    }

    /**
     * 动态接收ajax请求
     * url地址: /findAjax
     * 返回值:  List<User>  JSON串
     */
    @RequestMapping("/findAjax")
    @ResponseBody
    public List<User> findAjax(){

        return userService.findAll();
    }







}
